%if 0%{?fedora}
%global common_configure cinnamon,gnome-shell,gtk2,gtk3,gtk4,metacity,plank,xfwm
%else
%global common_configure gnome-shell,gtk2,gtk3,metacity,xfwm
%endif
%global _vpath_srcdir ..

%global common_desc							\
A flat theme with transparent elements.                                 \
Based on the Arc theme: https://github.com/jnsh/arc-theme	        \

%global common_name SolArc

%global solarc_version master
%global arc_version    20221218


Name:		solarc-theme
Version:	%{arc_version}
Release:	1%{?dist}
Summary:	Flat theme with transparent elements

License:	GPLv3+
URL:		https://github.com/schemar/%{name}
Source0:	%{url}/archive/%{solarc_version}.tar.gz

BuildArch:	noarch

BuildRequires:	fdupes
BuildRequires:	gtk3-devel
BuildRequires:	gtk-murrine-engine
BuildRequires:	inkscape
BuildRequires:  make
BuildRequires:	optipng
BuildRequires:	sassc
BuildRequires:	gnome-shell
BuildRequires:  wget
BuildRequires:  make
BuildRequires:  meson

%if 0%{?fedora}
BuildRequires:	cinnamon
BuildRequires:	gtk4-devel
%endif

Requires:	filesystem
Requires:	gnome-themes-extra
Requires:	gtk-murrine-engine

%description
%{common_desc}


%package plank
Summary:	%{common_name}-theme for Plank dock

Requires:	%{name}	== %{version}-%{release}

%if 0%{?fedora}
Requires:	plank

Supplements:	(%{name} and plank)
%endif

%description plank
%{common_desc}

This package contains the %{summary}.


%prep
%setup -n %{name}-%{solarc_version}
env ARCVERSION=%{arc_version} ./solarize.sh


%build
cd arc-theme-%{arc_version}
%{__mkdir} -p regular solid

pushd regular
%meson -Dthemes=%{common_configure}
%meson_build
popd

pushd solid
%meson -Dthemes=%{common_configure} -Dtransparency=false
%meson_build
popd


%install
cd arc-theme-%{arc_version}
pushd regular
%meson_install
popd

pushd solid
%meson_install
popd

# Install Plank-theme.
%{__mkdir} -p %{buildroot}/%{_datadir}/plank/themes/%{common_name}
%{__install} -pm 0644 common/plank/dock.theme	\
	%{buildroot}/%{_datadir}/plank/themes/%{common_name}
%{__install} -pm 0644 common/plank/dock-lighter.theme	\
	%{buildroot}/%{_datadir}/plank/themes/%{common_name}

# Link duplicate files.
%fdupes -s %{buildroot}%{_datadir}


%files
%license COPYING
%doc README.md
%{_datadir}/themes/*

%files plank
%if 0%{?fedora}
%{_datadir}/plank/themes/*
%else
%{_datadir}/plank
%endif


%changelog
* Thu Jun 01 2023 Matyáš Kroupa <kroupa.matyas@gmail.com> - 20221218-1
- Update to arc version 20221218

* Thu Apr 07 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 20220405-1
- Update to arc version 20220405

* Wed Feb 23 2022 Matyáš Kroupa <kroupa.matyse@gmail.com> - 20220223-1
- Update to arc version 20220223

* Mon Feb 08 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 20220105-2
- Remove gtk4, cinnamon and plank variants from epel
- Fix meson src directory

* Mon Feb 07 2022 Matyáš Kroupa <kroupa.matyas@gmail.com> - 20220105-1
- Update arc version to 20220105
- Update specfile to meson build system

* Sat Mar 06 2021 Matyáš Kroupa - 20210127-1
- Update to 20210127

* Fri Nov 13 2020 Matyáš Kroupa - 20201013-1
- Initial version
